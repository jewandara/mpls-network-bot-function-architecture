from core_configuration.config_bgp import *
from core_configuration.config_interface import *
from core_configuration.config_ipv4 import *
from core_configuration.config_isis import *
from core_configuration.config_l2vpn import *
from core_configuration.config_map import *
from core_configuration.config_multicast import *
from core_configuration.config_pim import *
from core_configuration.config_policy import *
from core_configuration.config_static import *
from core_configuration.config_vlan import *
from core_configuration.config_vrf import *
from core_configuration.config_vrrp import *


__version__ = "2.0.1"
__all__ = (
    "*.py",
)
