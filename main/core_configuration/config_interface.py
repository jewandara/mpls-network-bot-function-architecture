#################################################################
######################## Sira Version 2.0.1 #####################
#################################################################

import re
from mongodb import mongodb
import datetime


class saveBeInterface:
    def __init__(self, config_data, device_data):
        config_list = []                                                # SAVE CONFIGURATION DATA TO A ARRAY
        REPORT_DATE = str(datetime.datetime.now().date())               # GET REPORING DATE
        for i in range(len(config_data)):
            CONFIG_SET = 'interface '+str(config_data[i])+"\n!"
            CONFIG_SET = re.sub(r'             !', '!', CONFIG_SET)
            INTERFACE_DATA = re.findall('interface (.*?)\n!', CONFIG_SET, re.DOTALL)
            for j in range(len(INTERFACE_DATA)):
                CONFIG = 'interface '+str(INTERFACE_DATA[j])+"\n!"
                CONFIG = re.sub(r'             !', '!', CONFIG)
                config_list.append({ "did" : str(device_data['id']), "dip" : str(device_data['ip']), "type": "interface", "configuration" : str(CONFIG), "date" : str(REPORT_DATE) })
        mondb = mongodb("mpls", "core_be_configuration_log")
        x = mondb.addList(config_list)


class savePeInterface:
    def __init__(self, config_data, device_data):
        config_list = []                                                # SAVE CONFIGURATION DATA TO A ARRAY
        REPORT_DATE = str(datetime.datetime.now().date())               # GET REPORING DATE
        for i in range(len(config_data)):
            CONFIG = 'interface '+str(config_data[i])+"\n!"
            config_list.append({ "did" : str(device_data['id']), "dip" : str(device_data['ip']), "type": "interface", "configuration" : str(CONFIG), "date" : str(REPORT_DATE) })
        mondb = mongodb("mpls", "core_pe_configuration_log")
        x = mondb.addList(config_list)


class savePeServiceInstance:
    def __init__(self, config_data, device_data):
        config_list = []                                                # SAVE CONFIGURATION DATA TO A ARRAY
        REPORT_DATE = str(datetime.datetime.now().date())               # GET REPORING DATE
        for i in range(len(config_data)):
            CONFIG = 'service instance '+str(config_data[i])+"\n!"
            config_list.append({ "did" : str(device_data['id']), "dip" : str(device_data['ip']), "type": "service_instance", "configuration" : str(CONFIG), "date" : str(REPORT_DATE) })
        mondb = mongodb("mpls", "core_pe_configuration_log")
        x = mondb.addList(config_list)



