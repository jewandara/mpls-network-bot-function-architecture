#################################################################
######################## Sira Version 2.0.1 #####################
#################################################################

import re
from mysqldb import mysqldb
from mongodb import mongodb
import logmsg
import datetime

class saveBeMulticastRouting:
    def __init__(self, config_data, device_data):
        config_list = []                                                # SAVE CONFIGURATION DATA TO A ARRAY
        REPORT_DATE = str(datetime.datetime.now().date())               # GET REPORING DATE
        for i in range(len(config_data)):
            CONFIG_SET = 'multicast-routing '+str(config_data[i])+"\n!"
            CONFIG_SET = re.sub(r'             !', '!', CONFIG_SET)
            config_list.append({ "did" : str(device_data['id']), "dip" : str(device_data['ip']), "type": "multicast_routing", "configuration" : str(CONFIG_SET), "date" : str(REPORT_DATE) })
        mondb = mongodb("mpls", "core_be_configuration_log")
        x = mondb.addList(config_list)
        #print(config_list)
        #print(x)


class savePeMulticastRouting:
    def __init__(self, config_data, device_data):
        config_list = []                                                # SAVE CONFIGURATION DATA TO A ARRAY
        REPORT_DATE = str(datetime.datetime.now().date())               # GET REPORING DATE
        for i in range(len(config_data)):
            CONFIG_SET = 'multicast-routing '+str(config_data[i])+"\n!"
            config_list.append({ "did" : str(device_data['id']), "dip" : str(device_data['ip']), "type": "multicast_routing", "configuration" : str(CONFIG_SET), "date" : str(REPORT_DATE) })
        mondb = mongodb("mpls", "core_pe_configuration_log")
        x = mondb.addList(config_list)
        #print(config_list)
        #print(x)

