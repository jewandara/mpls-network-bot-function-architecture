#################################################################
######################## Sira Version 2.0.1 #####################
#################################################################

import re
from mysqldb import mysqldb
from mongodb import mongodb
import logmsg
import datetime

class saveBeRouterStatic:
    def __init__(self, config_data, device_data):
        config_list = []                                                # SAVE CONFIGURATION DATA TO A ARRAY
        REPORT_DATE = str(datetime.datetime.now().date())               # GET REPORING DATE
        for i in range(len(config_data)):
            CONFIG_SET = 'router static '+str(config_data[i])+"\n!"
            CONFIG_SET = re.sub(r'             !', '!', CONFIG_SET)
            config_list.append({ "did" : str(device_data['id']), "dip" : str(device_data['ip']), "type": "router_static", "configuration" : str(CONFIG_SET), "date" : str(REPORT_DATE) })
        mondb = mongodb("mpls", "core_be_configuration_log")
        x = mondb.addList(config_list)
        #print(config_list)
        #print(x)


class savePeRouterStatic:
    def __init__(self, config_data, device_data):
        config_list = []                                                # SAVE CONFIGURATION DATA TO A ARRAY
        REPORT_DATE = str(datetime.datetime.now().date())               # GET REPORING DATE
        for i in range(len(config_data)):
            CONFIG_SET = 'router static '+str(config_data[i])+"\n!"
            CONFIG_SET = re.sub(r'             !', '!', CONFIG_SET)
            config_list.append({ "did" : str(device_data['id']), "dip" : str(device_data['ip']), "type": "router_static", "configuration" : str(CONFIG_SET), "date" : str(REPORT_DATE) })
        mondb = mongodb("mpls", "core_pe_configuration_log")
        x = mondb.addList(config_list)
        #print(config_list)
        #print(x)

