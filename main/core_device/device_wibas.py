#################################################################
######################## Sira Version 2.0.1 #####################
#################################################################

from netmiko import ConnectHandler
import re
from mysqldb import mysqldb
from mongodb import mongodb
import write
import datetime
import validate
import core_configuration
import logmsg
import os

class __DEVICE:
    def __init__(self, _JUMP_SERVER_CONFIG, _DOMAIN_USER_CONFIG, _DEVICE_DATA):
        try:
            # CONFIGERATION DECLARATION
            self.CONFIG_TEXT = ""
            self.JUMP_SERVER_CONFIG_IN = _JUMP_SERVER_CONFIG["in"]
            self.JUMP_SERVER_CONFIG_OUT = _JUMP_SERVER_CONFIG["out"]
            self.DOMAIN_USER_CONFIG_USERNAME = _DOMAIN_USER_CONFIG["username"]
            self.DOMAIN_USER_CONFIG_PASSWORD = _DOMAIN_USER_CONFIG["password"]
            self.DOMAIN_USER_CONFIG_SYNC_MYSQL_DB = _DOMAIN_USER_CONFIG["syncMysqlDB"]
            self.DOMAIN_USER_CONFIG_SYNC_MONGO_DB = _DOMAIN_USER_CONFIG["syncMongoDB"]
            self.JSERVER = []
            self.ENABLE = []
            # DEVICE DATA DECLARATION
            self.DEVICE_ID = _DEVICE_DATA[0]
            self.DEVICE_TYPE = _DEVICE_DATA[1]
            self.DEVICE_IP = _DEVICE_DATA[2]
            self.DEVICE_NAME = _DEVICE_DATA[3]
            self.DEVICE_HOST_NAME = _DEVICE_DATA[4]
            self.DEVICE_BRAND = _DEVICE_DATA[5]
            self.DEVICE_CATEGORY = _DEVICE_DATA[6]
            self.DEVICE_POSITION = _DEVICE_DATA[7]
            self.DEVICE_SYMBOLE = _DEVICE_DATA[8]
            self.DEVICE_USER = _DEVICE_DATA[9]
            self.DEVICE_PASS = _DEVICE_DATA[10]
            self.DEVICE_ACTIVE = _DEVICE_DATA[11]
            self.DEVICE_STATUS = _DEVICE_DATA[12]
            self.DEVICE_UNIQUE_VENDOR = _DEVICE_DATA[1]
            self.LAG = logmsg.log_and_msg_EN(_DEVICE_DATA[2])
            # ADDITIONAL DECLARATION
            self.CONFIG_VRF = 0
            self.CONFIG_VLAN = 0
            self.CONFIG_IPV4 = 0
            self.CONFIG_IP_ROUTE_VRF = 0
            self.CONFIG_IP_ACCESS_LIST = 0
            self.CONFIG_IP_PREFIX_LIST = 0 
            self.CONFIG_CLASS_MAP = 0
            self.CONFIG_POLICY_MAP = 0
            self.CONFIG_INTERFACE = 0
            self.CONFIG_SERVICE_INSTANCE = 0
            self.CONFIG_L2VPN = 0
            self.CONFIG_ROUTE_POLICY = 0
            self.CONFIG_ROUTE_STATIC = 0
            self.CONFIG_ROUTE_ISIS = 0
            self.CONFIG_ROUTE_BGP = 0
            self.CONFIG_ROUTE_BGP_ADDRESS_FAMILY = 0
            self.CONFIG_ROUTE_VRRP = 0
            self.CONFIG_ROUTE_PIM = 0
            self.CONFIG_MULTICAST_ROUTING = 0
            self.LAG.writeLOG('INITIATION_D')
        except:
            self.LAG.writeLOG('INITIATION_E')

    #########################################################################
    def _JUMP(self):
        try:
            self.JSERVER = ConnectHandler(**self.JUMP_SERVER_CONFIG_IN)
            OUT = self.JSERVER.send_command_timing(' ', strip_command=False, strip_prompt=False)
            JUMP_AFTER_AT = validate.SPLIST("@", OUT)
            if(JUMP_AFTER_AT[1] == str(self.JUMP_SERVER_CONFIG_OUT)):
                self.LAG.writeLOG('LOGIN_JUMP_D', 'USER : '+str(self.JUMP_SERVER_CONFIG_IN['username']))
                return 1
            else:
                self.LAG.writeLOG('LOGIN_JUMP_F')
                return 0
        except:
            os.system('COLOR 4')
            self.LAG.writeLOG('LOGIN_JUMP_E')
            return 0

    #########################################################################
    def _TELNET(self):
        try:
            OUT = self.JSERVER.send_command_expect( 'telnet ' + str(self.DEVICE_IP), 'Username')
            OUT += self.JSERVER.send_command_expect( str(self.DEVICE_USER), 'Password')
            OUT += self.JSERVER.send_command_timing( str(self.DEVICE_PASS) + '\n', normalize=False)
            self.LAG.writeLOG('TELNET_D')
            return 1
        except:
            os.system('COLOR 4')
            self.LAG.writeLOG('TELNET_E')
            return 0


    #########################################################################
    def _DEACTIVE_DEVICE(self, COMMENT):
        try:
            db = mysqldb(str(self.DOMAIN_USER_CONFIG_SYNC_MYSQL_DB))
            RESULT = db.edit("mpls_core_device_tb", " coment='"+str(COMMENT)+"', active=0 ", " ip='"+str(self.DEVICE_IP)+"' ")
            if(RESULT[0]):
                self.LAG.writeLOG('DEACTIVE_DEVICE_D')
                return 1
            else:
                os.system('COLOR E')
                self.LAG.writeLOG('DEACTIVE_DEVICE_F')
                return 0
        except:
            os.system('COLOR 4')
            self.LAG.writeLOG('DEACTIVE_DEVICE_E')
            return 0

    #########################################################################
    def _COMMENT_DEVICE(self, COMMENT):
        try:
            db = mysqldb(str(self.DOMAIN_USER_CONFIG_SYNC_MYSQL_DB))
            RESULT = db.edit("mpls_core_device_tb", " coment='"+str(COMMENT)+"', status=1 ", " ip='"+str(self.DEVICE_IP)+"' ")
            if(RESULT[0]):
                self.LAG.writeLOG('COMMENT_DEVICE_D')
                return 1
            else:
                os.system('COLOR E')
                self.LAG.writeLOG('COMMENT_DEVICE_F')
                return 0
        except:
            os.system('COLOR 4')
            self.LAG.writeLOG('COMMENT_DEVICE_E')
            return 0

    #########################################################################
    def _CREATE_BACKUP(self):
        try:
            BACKUP = write.config(str(self.DEVICE_TYPE), str(self.DEVICE_IP)+"__"+str(self.DEVICE_BRAND)+"__"+str(self.DEVICE_CATEGORY), str(self.CONFIG_TEXT))
            if(BACKUP[0]):
                self.LAG.writeLOG('CREATE_BACKUP_C')
                return 1
            else:
                os.system('COLOR E')
                self.LAG.writeLOG('CREATE_BACKUP_F')
                return 0
        except:
            os.system('COLOR 4')
            self.LAG.writeLOG('CREATE_BACKUP_E')
            return 0

    #########################################################################
    def _SAVE_BACKUP(self):
        try:
            REPORTING_DATE = datetime.datetime.now().date()
            REPORTING_TIME = datetime.datetime.now().time()
            db = mongodb(str(self.DOMAIN_USER_CONFIG_SYNC_MONGO_DB), "core_"+str(self.DEVICE_UNIQUE_VENDOR)+"_config_log")
            SQL_DATA_STRING = {
                        'DID': str(self.DEVICE_ID), 'IP': str(self.DEVICE_IP), 'TYPE': str(self.DEVICE_TYPE),
                        'BRAND': str(self.DEVICE_BRAND), 'CATEGORY': str(self.DEVICE_CATEGORY), 'POSITION': str(self.DEVICE_POSITION),
                        'NAME': str(self.DEVICE_NAME), 'HOST_NAME': str(self.DEVICE_HOST_NAME), 'CONFIG': str(self.CONFIG_TEXT),
                        'REPORTING_DATE': str(REPORTING_DATE), 'REPORTING_TIME': str(REPORTING_TIME)
                        }
            BACKUP = db.add(SQL_DATA_STRING)
            if(BACKUP[0]):
                self.LAG.writeLOG('SAVE_BACKUP_D')
                return 1
            else:
                os.system('COLOR E')
                self.LAG.writeLOG('SAVE_BACKUP_F')
                return 0
        except:
            os.system('COLOR 4')
            self.LAG.writeLOG('SAVE_BACKUP_E')
            return 0
        
    #########################################################################
    def _ERROR_LOG_PRINT(self, CODE, TYPE = "C"):
        self.LAG.writeLOG(str(CODE))
        CODE_MESSAGE = self.LAG.getMSG(str(CODE))
        if(TYPE == "A"):
            os.system('COLOR 1')
            self._DEACTIVE_DEVICE(str(CODE_MESSAGE))
            print('▀▀----------------------------------------------------------------------▀▀')
            print('▀▀           * * *  MPLS NETWORK BACKUP ERROR FOUND  * * *              ▀▀')
            print('▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀')
        else:
            os.system('COLOR 1')
            self._COMMENT_DEVICE(str(CODE_MESSAGE))
            print('▀▀----------------------------------------------------------------------▀▀')
            print('▀▀           * * *  MPLS NETWORK BACKUP ERROR FOUND  * * *              ▀▀')
            print('▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀')
        return 0












    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################
    ##########################  DEFAULT FUNCTIONS  ##########################
    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################


    #########################################################################
    def _RUNNING_CURRENT_CONFIG(self):
        try:
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_S')
            OUT = self.JSERVER.send_command_timing("get aaa group\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get aaa local\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get aaa tacserver\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get aaa user\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get alarm supportedlist\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get alarm activelist\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get alarm stats\n", normalize=False)
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_S')
            OUT += self.JSERVER.send_command_timing("get bridge cfg\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge cvlanmap\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge l2port\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge l2portstats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge mac-security\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge mac-security-stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge split-horizon\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge split-horizon-stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge storm-ctrl\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge storm-ctrl-stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge vlan\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge vlan-stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge vlanport\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get bridge vlanport-stats\n", normalize=False)
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_S')
            OUT += self.JSERVER.send_command_timing("get cfm defmd\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get cfm intf\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get cfm defmdentry\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get cfm ma\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get cfm md\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get cfm mep\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get cfm mepstats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get cfm state\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get cos bwprof\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get cos bwprof-stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get efm config\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get efm discovery\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get efm remote-loopback\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get efm stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get efm linkmonitor\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get efm linkmonitor-event-log\n", normalize=False)
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_S')
            OUT += self.JSERVER.send_command_timing("get ethernet hist-stats status\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get ethernet loopback\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get ethernet state\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get ethernet stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get fdb dynamic\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get fdb static\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get l2cp cfg\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get l2cp protocol\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get license info\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get ntp client\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get rmon stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get security passreqs\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get security acl\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get security level\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get sensor hist-stats sensor thermo\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get snmp cfg\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get snmp community\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get syslog client\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get system alarm\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get system datetime\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get system info\n", normalize=False)
            #OUT += self.JSERVER.send_command_timing("get system log module local3\n", normalize=False)
            #OUT += self.JSERVER.send_command_timing("get system log module auth\n", normalize=False)
            #OUT += self.JSERVER.send_command_timing("get system log module kern\n", normalize=False)
            #OUT += self.JSERVER.send_command_timing("get system log module err\n", normalize=False)
            #OUT += self.JSERVER.send_command_timing("get system log module rest\n", normalize=False)
            #OUT += self.JSERVER.send_command_timing("get system log module traps\n", normalize=False)
            #OUT += self.JSERVER.send_command_timing("get system log module actions\n", normalize=False)
            #OUT += self.JSERVER.send_command_timing("get system log module wmacutr\n", normalize=False)
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_S')
            OUT += self.JSERVER.send_command_timing("get system mng\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get system params\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get system route\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get system services\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get system sw\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get trap destn\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get trap list\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get trap rule\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get tty actsessions\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get tty sessions\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get tty ssh\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get tty telnet\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wmac tsconf\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wmac sf-status\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wmac status\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wmac stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wmac classrule\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wmac sf-stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wmac sf-hist-stats status\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wmac ts-utr\n", normalize=False)
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_S')
            OUT += self.JSERVER.send_command_timing("get wphy alarmadmin\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy alarm-link-thresh\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy modemconf\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy modemlinkstats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy modemlinkstatus\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy modemlink-hist-stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy modemphymode\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy modemprof\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy modemprofcap\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy modemstatus\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy rfucap\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy rfuinfo\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy rfuscan\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy rfustatus\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get wphy rfu-hist-stats\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("get y1731 lmcos\n", normalize=False)
            self.CONFIG_TEXT = self.CONFIG_TEXT + "<GET>"+str(OUT)+"</GET>"
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_C', 'GET FUNCTION')
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_S')
            OUT = self.JSERVER.send_command_timing("config show antenna\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("config show backup local\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("config show bridge\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("config show current\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("config show efm\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("config show oam\n", normalize=False)
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_S')
            OUT += self.JSERVER.send_command_timing("config show saved\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("config show syslog\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("config show tacacs\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("config show userdefault\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("config show wmac\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("config show wphy\n", normalize=False)
            self.CONFIG_TEXT = self.CONFIG_TEXT + "<CONFIG>"+str(OUT)+"</CONFIG>"
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_C', 'CONFIG FUNCTION')
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_S')
            OUT = self.JSERVER.send_command_timing("ver\n", normalize=False)
            self.CONFIG_TEXT = self.CONFIG_TEXT + str(OUT)
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_C')
            return 1
        except:
            self.LAG.writeLOG('RUNNING_CURRENT_CONFIG_E')
            return 0


#########################################################################
#########################################################################
#########################################################################
#########################################################################
#########################################################################
#########################################################################
############################  MAIN FUNCTION  ############################
#########################################################################
#########################################################################
#########################################################################
#########################################################################
#########################################################################
#########################################################################
class WIBAS(__DEVICE):
    def main(self):
        if(self._JUMP()):
            if(self._TELNET()):
                if(self._RUNNING_CURRENT_CONFIG()):
                    if(self._CREATE_BACKUP()):
                        if(self._SAVE_BACKUP()):
                            print('▀▀----------------------------------------------------------------------▀▀')
                            print('▀▀                   MPLS NETWORK BACKUP COMPLETE                       ▀▀')
                            print('▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀')
                            return 1
                        else: return self._ERROR_LOG_PRINT('SAVE_BACKUP_N')
                    else: return self._ERROR_LOG_PRINT('CREATE_BACKUP_N')
                else: return self._ERROR_LOG_PRINT('RUNNING_CURRENT_CONFIG_N', 'A')
            else: return self._ERROR_LOG_PRINT('TELNET_N', 'A')
        else: return self._ERROR_LOG_PRINT('LOGIN_JUMP_N')








    

